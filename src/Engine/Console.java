package Engine;

import java.awt.Color;

import acm.graphics.*;
import acm.program.*;


public class Console extends Graphics {

	public static final int WIDTH = 1080;
	public static final int HEIGHT = WIDTH / 8 * 5;
	private Menu menu;
	private Instructions instruct;
	private Game game;
	boolean runable = false;
	public static final int RefreshRate = 50;
	
	public void init()
	{
		setSize(WIDTH, HEIGHT);
		addKeyListeners();
		addMouseListeners();
		menu = new Menu(this);
		instruct = new Instructions(this);
		game = new Game(this);
		SwitchScreen(menu);
	}
	
    public void run()
    {
    	while (true)
    	{
    		pause(RefreshRate);
    		current.run();
    	}
    }
    
    public void toMenu()
    {
    	pause(RefreshRate);
		SwitchScreen(menu);
    }
    
    public void toInstruction()
    {
		SwitchScreen(instruct);
    }
    
    public void toGame()
    {
    	SwitchScreen(game);
    }
}
