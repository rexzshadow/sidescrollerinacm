package Engine;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import Stuff.*;
import acm.graphics.*;
import acm.program.*;

public class Game extends Screen{
	public static final int RefreshRate = 300;
	double x = 0, y = 0;
	GRect rect;
	Console console;
	boolean run = false;
	public Player player;
	private ArrayList<Tiles> tiles;
	private boolean gravity = true;
	GImage background = new GImage("background.png");
	boolean canMove = false;
	int check = 0;
	private ArrayList<Enemy> enemies;
	private ArrayList<GImage> lifeCounter;
	public Game(Console console) {
		// TODO Auto-generated constructor stub
		//rect = new GRect(60, 60);
		//rect.setColor(Color.RED);
		this.console = console;
		player = new Player("megaman1", 10, 400, 10, this, console);
		tiles = new ArrayList<Tiles>();
		enemies = new ArrayList<Enemy>();
		lifeCounter = new ArrayList<GImage>();
		for(int i = 0; i < 6; i++)
		{
			lifeCounter.add(new GImage("heart.png"));
			lifeCounter.get(i).setLocation(32*i, 0);
		}
		for(int i = 0; i < 40; i++)
		{
			tiles.add(new Tiles("grass", x, console.HEIGHT - 52, x, this, console));
			x += 53;
		}
		enemies.add(new Enemy("megaman2", 600, 400, 600, this, console));
		player.getTile();
		player.getEnemy();
		for(Enemy e : enemies)
		{
			e.getTile();
		}
	}

	@Override
	public void run()
	{
		if(run)
		{
			canMove = player.onGround();
			if(canMove && check == 1)
			{
				player.moveX(0);
			}
			player.tick();
		}
		for(Tiles t : tiles)
		{
			t.update();
		}
		for(Enemy e : enemies)
		{
			e.tick();
		}
		updateLife();
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		//console.remove(rect);
		console.remove(player.rImage());
		run = false;
		for(Tiles t : tiles)
		{
			console.remove(t.rImage());
		}
		for(Enemy e : enemies)
		{
			console.remove(e.rImage());
		}
		console.remove(background);
		for(int i = 0; i < 6; i++)
		{
			console.remove(lifeCounter.get(i));
		}
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		//console.add(rect);
		console.add(background);
		for(int i = 0; i < player.getLife(); i++)
		{
			console.add(lifeCounter.get(i));
		}
		for(Tiles t : tiles)
		{
			console.add(t.rImage());
		}
		for(Enemy e : enemies)
		{
			console.add(e.rImage());
		}
		console.add(player.rImage());
		run = true;
	}
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
		{
			console.toMenu();
		}
		if(canMove)
		{
			if(e.getKeyCode() == KeyEvent.VK_RIGHT)
			{
				player.moveX(5);
				check = 0;
			}
		
			if(e.getKeyCode() == KeyEvent.VK_LEFT)
			{
				player.moveX(-5);
				check = 0;
			}
		
			if(e.getKeyCode() ==KeyEvent.VK_SPACE)
			{
				player.jump(150);
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_RIGHT)
		{
			player.side = 0;
		}
	
		if(e.getKeyCode() == KeyEvent.VK_LEFT)
		{
			player.side = 1;
		}
	}
	
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		if(canMove)
		{
			if(e.getKeyCode() == KeyEvent.VK_RIGHT)
			{
				player.moveX(0);
			}
		
			if(e.getKeyCode() == KeyEvent.VK_LEFT)
			{
				player.moveX(0);
			}
		}
		
		if(e.getKeyCode() == KeyEvent.VK_RIGHT && !canMove)
		{
			check = 1;
		}
		
		if(e.getKeyCode() == KeyEvent.VK_LEFT && !canMove)
		{
			check = 1;
		}
		
	}
	
	public ArrayList<Tiles> getTiles()
	{
		return tiles;
	}
	
	public ArrayList<Enemy> getEnemies()
	{
		return enemies;
	}
	
	void updateLife()
	{
		for(GImage l : lifeCounter)
		{
			console.remove(l);
		}
		for(int i = 0; i < player.getLife(); i++)
		{
			console.add(lifeCounter.get(i));
		}
		
		if(player.getLife() == 0)
		{
			console.toMenu();
		}
	}
}
