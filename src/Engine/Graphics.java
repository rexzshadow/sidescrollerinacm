package Engine;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import acm.program.*;
import acm.graphics.*;

public class Graphics extends GraphicsProgram{
	
	protected Screen current;
	
	protected void SwitchScreen(Screen newScreen)
	{
		if(current != null)
		{
			current.hide();
		}
		System.out.println("display!");
		newScreen.show();
		current = newScreen;
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		current.mousePressed(e);
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		current.mouseReleased(e);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		current.mouseClicked(e);
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		current.mouseDragged(e);
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		current.mouseMoved(e);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		current.keyPressed(e);
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		current.keyReleased(e);
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		current.keyTyped(e);
	}

}
