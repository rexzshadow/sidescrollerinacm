package Engine;
import java.awt.Color;
import java.awt.event.KeyEvent;

import acm.graphics.*;
public class Instructions extends Screen {
	GRect rect;
	Console console;
	public Instructions(Console console) {
		// TODO Auto-generated constructor stub
		rect = new GRect(60, 60);
		rect.setColor(Color.BLACK);
		this.console = console;
	}

	public void run()
	{
		
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		console.remove(rect);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		console.add(rect);
	}
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
		{
			console.toMenu();
		}
	}
}
