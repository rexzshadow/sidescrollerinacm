package Engine;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import acm.graphics.*;

public class Menu extends Screen{

	private GRect[] choice;
	Console console;
 	private GLabel[] label;

	
	public Menu(Console console) {
		// TODO Auto-generated constructor stub
		choice =  new GRect[3];
		label = new GLabel[3];
		label[0] = new GLabel("Play!");
		label[1] = new GLabel("Instruction!");
		label[2] = new GLabel("Exit!");
		for(int i = 0; i < 3; i++)
		{
			choice[i] = new GRect(200, 50);
			choice[i].setLocation(500, i * 100 + 150);
			choice[i].setColor(Color.BLUE);
			label[i].setLocation(570, i * 100 + 175);
		}
		this.console = console;
	}

	public void run()
	{
		
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 3; i++)
		{
			console.remove(choice[i]);
			console.remove(label[i]);
		}
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		for(int i = 0; i < 3; i++)
		{
			console.add(choice[i]);
			console.add(label[i]);
		}
	}

	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		/*System.out.println(choice[1].getLocation().toPoint());
		System.out.println(e.getPoint());
		GObject temp = console.getElementAt(e.getX(), e.getY());
		if (temp == choice[0])
		{
			System.out.println("Game Start!");
			console.toGame();
		}
		
		if(temp == choice[1]);
		{
			System.out.println("Instruction Start!!");
			console.toInstruction();
		}*/
	}
	
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_1)
		{
			console.toGame();
		}
		else if(e.getKeyCode() == KeyEvent.VK_2)
		{
			console.toInstruction();
		}
	}
	

}
