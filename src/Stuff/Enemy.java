package Stuff;

import Engine.Console;
import Engine.Game;

public class Enemy extends Mob{

	boolean canMove;
	public Enemy(String name, double x, double y, double globX, Game state,
			Console console) {
		super(name, x, y, globX, state, console);
		// TODO Auto-generated constructor stub
		canMove = false;
		life = 3;
	}
	
	public void tick()
	{
		falling();
		super.move();
		move();
		spirit.setLocation(x, y);
	}
	
	public void move()
	{
		if(!inAir)
		{
			x -= 4;
		}
	}

}
