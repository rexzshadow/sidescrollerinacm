package Stuff;

import acm.graphics.*;
import Engine.*;
import Render.*;

import java.awt.event.KeyEvent;
public abstract class Entities {
	public boolean onGround;
	protected double x, y, globX, globY, lastX;
	protected int life;
	boolean wasHit;
	protected Game state;
	protected GImage spirit;
	protected Console console;
	
	public Entities(String name, double x, double y, double globX, Game state, Console console)
	{
		name = name + ".png";
		spirit = new GImage(name);
		this.x = x;
		this.y = y;
		this.globX = globX;
		this.state = state;
		spirit.setLocation(x, y);
		this.console = console;
	}
	
	public abstract void tick();
	
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void keyTyped(KeyEvent e) {
		
	}
	
	public GImage rImage()
	{
		return spirit;
	}
}
