package Stuff;

import java.util.ArrayList;

import acm.graphics.GObject;
import Engine.Console;
import Engine.Game;

public abstract class Mob extends Entities{

	GObject bound;
	protected double dx = 0, dy = 0;
	protected double grav;
	protected boolean inAir;
	protected boolean jump;
	protected double maxDY;
	ArrayList<Tiles> tiles;
	ArrayList<Enemy> enemies;
	public Mob(String name, double x, double y, double globX, Game state,
			Console console) {
		super(name, x, y, globX, state, console);
		grav = 0.5;
		maxDY = 7;
		
	}
	@Override
	public void tick() {
		move();
		falling();
		//offScreen();
	}
	
	public void move()//move character
	{
		lastX = x;
		if(!hasECollision() || hasVCollision())
		{
			x += dx;
		}

		if(!hasVCollision())
		{
			y += dy;
		}
		globX += x - lastX;
	}
	
	protected boolean hasVCollision()//check if any vert collision
	{
		bound = console.getElementAt(x, y + spirit.getHeight());

		for(Tiles t: tiles)//check for the ground tiles
		{
			if(bound == t.rImage())
			{
				dy = 0;
				inAir = false;
				jump = true;
				return true;
			}
		}
		inAir = true;
		jump = false;
		return false;
	}
	
	protected boolean hasECollision()//check for horizontal collision
	{
		return false;
	}
	
	protected void falling()//apply gravity
	{
		if(inAir)
		{
			dy += grav;
			if(dy > maxDY)
			{
				dy = maxDY;
			}
		}
	}
	
	public void jump(double jumpH)//apply jump
	{
		if(jump)
		{
			System.out.println("Jump");
			y -= jumpH;
		}
	}
	
	public int getLife()
	{
		return life;
	}
	
	public void getTile()
	{
		tiles = state.getTiles();
	}
	
	public void getEnemy()
	{
		enemies = state.getEnemies();
	}
}
