package Stuff;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import acm.graphics.*;
import Engine.Console;
import Engine.Game;

public class Player extends Mob{

	public boolean onGround;
	public int side = 0;
	private int check = 0;
	double speedX, speedY;
	GImage spirit1,spirit2;
	public Player(String name, double x, double y, double globX, Game state, Console console) {
		super(name, x, y, globX, state, console);
		// TODO Auto-generated constructor stub
		spirit2 = new GImage("megaman2.png");
		spirit1 = new GImage("megaman1.png");
		spirit1.setLocation(x , y);
		spirit2.setLocation(x , y);
		life = 6;
	}

	public void tick() {
		/*if(side == 0)
		{
			spirit = spirit1;
		}
		else
		{
			spirit = spirit2;
		}*/
		move();
		falling();
		spirit1.setLocation(x , y);
		spirit2.setLocation(x , y);
		spirit.setLocation(x , y);
	}
	
	public void move()
	{
		lastX = x;
		//haveECollision();
		if(globX < Console.WIDTH / 2)//moving while screen lockd
		{
			if(!hasECollision() || hasVCollision())
			{
				
				x += dx;
			}
			globX += x - lastX;
		}
		else//move while scrolling
		{
			globX += dx;
		}
		
		if(!hasVCollision())//jump
		{
			y += dy;
		}
	}
	
	public GImage rImage()
	{
		return spirit;
	}
	
	public void moveX(double x)
	{
		this.dx = x;
	}
	
	public void moveY(double y)
	{
		this.dy = y;
	}
	
	protected boolean hasECollision()//check for horizontal collision
	{
		bound = console.getElementAt(x + spirit.getWidth(), y + spirit.getHeight()/ 2);
		for(Enemy e : enemies)
		{
			if(bound == e.rImage())
			{
				System.out.println("Hit by Enemy!");
				dx = -10;
				life--;
				return true;
			}
			
		}
		return false;
	}
	
	public boolean onGround()
	{
		return !inAir;
	}
	
	public double getX()
	{
		return globX;
	}
	
	
}
